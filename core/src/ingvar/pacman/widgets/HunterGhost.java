package ingvar.pacman.widgets;

import com.badlogic.gdx.math.Vector2;
import ingvar.pacman.ai.PathFindingFactory;
import ingvar.pacman.ai.PathNode;
import ingvar.pacman.utils.ObjectPool;

/**
 * Created by Igor_Zubenko on 16/07/2014.
 */
public class HunterGhost extends Ghost {

    public HunterGhost() {
        super(GhostType.HUNTER);
    }

    @Override
    protected void setTrack() {
        Vector2 pacmanPos = pacman.getCurrentCell();
        if(target == null || !target.isSame(pacmanPos)) {
            target = ObjectPool.getInstance().getPathNode().set(pacmanPos.x, pacmanPos.y);
            ObjectPool.getInstance().free(pacmanPos);
            if(path != null) {
                ObjectPool.getInstance().free(path, true);
            }
            path = null;
        }
        if(path == null) {
            Vector2 ghostPos = getCurrentCell();
            PathNode origin = ObjectPool.getInstance().getPathNode().set(ghostPos.x, ghostPos.y);
            path = PathFindingFactory.getInstance().findByAStar(maze, origin, target, checker);
            ObjectPool.getInstance().free(ghostPos);
        }
    }

}
