package ingvar.pacman.widgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import ingvar.pacman.ai.PathFindingFactory;
import ingvar.pacman.ai.PathNode;
import ingvar.pacman.utils.ObjectPool;

/**
 * Created by Igor Zubenko on 16.07.2014.
 */
public class GoblinGhost extends Ghost {

    private float dummyTime;
    private Vector2 lastPos;

    public GoblinGhost() {
        super(GhostType.GOBLIN);
        speed = Pacman.SPEED * .95f;
        dummyTime = 0;
    }

    @Override
    protected void setTrack() {
        Vector2 cur = getCurrentCell();
        if((target == null || target.isSame(cur)) && isCentered()) {
            Vector2 trg = getRandomTarget();
            target = ObjectPool.getInstance().getPathNode().set(trg.x, trg.y);
            ObjectPool.getInstance().free(trg);
            if(path != null) {
                ObjectPool.getInstance().free(path, true);
            }
            path = null;
            setDirection(null);
        }
        if(path == null) {
            PathNode origin = ObjectPool.getInstance().getPathNode().set(cur.x, cur.y);
            path = PathFindingFactory.getInstance().findByAStar(maze, origin, target, checker);
        }
        ObjectPool.getInstance().free(cur);
    }

    @Override
    protected void chase() {
        if(isMortal()) {
            setTrack(); //TODO: escape
        } else {
            setTrack();
        }

        Vector2 pos = getCurrentCell();
        if(path != null) {
            if(getDirection() == null || !getDirection().equals(path.d)) {
                setDirection(path.d);
            }
            if(path.isSame(pos)) {
                path = path.next;
            }
        } else {
            setPosition(pos.x * tileSize, pos.y * tileSize);
        }
        Vector2 ppos = pacman.getCurrentCell();
        if(!isMortal() && isAlive() && pos.equals(ppos)) {
            interaction.onEatPacman();
        }
        if(!pos.equals(lastPos)) {
            lastPos = ObjectPool.getInstance().getVector2().set(pos);
        } else {
            dummyTime += Gdx.graphics.getDeltaTime();
        }
        if(dummyTime >= 2) {
            resetChasing();

            dummyTime = 0;
            ObjectPool.getInstance().free(lastPos);
            lastPos = null;
        }

        ObjectPool.getInstance().free(ppos);
        ObjectPool.getInstance().free(pos);
    }

    private Vector2 getRandomTarget() {
        int posX = MathUtils.random(maze.getWidth() - 1);
        int posY = MathUtils.random(maze.getHeight() - 1);
        Vector2 pos = ObjectPool.getInstance().getVector2().set(posX, posY);
        return checker.isMovable(posX, posY, maze.getCell(posX, posY)) ? pos : getRandomTarget();
    }

    private boolean isCentered() {
        Vector2 curr = getCurrentCell();
        boolean result = MathUtils.isEqual(getX(), curr.x * tileSize, tileSize * .05f)
                && MathUtils.isEqual(getY(), curr.y * tileSize, tileSize * .05f);
        ObjectPool.getInstance().free(curr);
        return result;
    }

}
