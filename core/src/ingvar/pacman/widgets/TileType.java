package ingvar.pacman.widgets;

/**
 * Created by Igor_Zubenko on 15/07/2014.
 */
public enum TileType {

    EMPTY(1),
    DOT_SMALL(2),
    DOT_BIG(3),
    DOOR(4),
    WALL(5);

    public final int value;

    TileType(int value) {
        this.value = value;
    }

}
