package ingvar.pacman.widgets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import ingvar.pacman.ai.CellChecker;
import ingvar.pacman.utils.ObjectPool;

/**
 * Created by Igor_Zubenko on 15/07/2014.
 */
public abstract class Creature extends Widget {

    protected TiledMapTileLayer maze;
    protected TiledMapTileSet tiles;
    protected CellChecker checker;
    protected float speed;
    protected float tileSize;
    private Direction direction;

    private float rotation;
    private float stateTime;

    public Creature() {
        speed = 0;
        rotation = 0;
        stateTime = 0;
        setDirection(Direction.RIGHT);
        checker = new CellChecker() {
            @Override
            public boolean isMovable(int x, int y, TiledMapTileLayer.Cell cell) {
                return cell.getTile().getId() <= TileType.DOT_BIG.value;
            }
        };
    }

    public void initMovement(TiledMapTileLayer m, TiledMapTileSet t, float ts) {
        maze = m;
        tiles = t;
        this.tileSize = ts;
        addAction(Actions.forever(new Action() {
            @Override
            public boolean act(float delta) {
                move();
                return true;
            }
        }));
    }

    public Direction getDirection () {
        return direction;
    }

    public boolean setDirection(Direction d) {
        boolean isAvailable = true;
        if(maze != null && tiles != null && d != null) {
            Vector2 nextCell = getNextCell(d);
            boolean onPosition = false;
            switch (d) {
                case LEFT:
                case RIGHT:
                    onPosition = MathUtils.isEqual(getY(), nextCell.y * tileSize, 1);
                    break;
                case UP:
                case DOWN:
                    onPosition = MathUtils.isEqual(getX(), nextCell.x * tileSize, 1);
                    break;
            }
            TiledMapTileLayer.Cell next = maze.getCell((int)nextCell.x, (int)nextCell.y);
            if(onPosition && checker.isMovable((int)nextCell.x, (int)nextCell.y, next)) {
                this.direction = d;
            } else {
                isAvailable = false;
            }
            ObjectPool.getInstance().free(nextCell);
        } else {
            this.direction = d;
        }
        return isAvailable;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public Vector2 getCurrentCell() {
        return ObjectPool.getInstance().getVector2().set((int) (getCenterX() / tileSize), (int) (getCenterY() / tileSize));
    }

    public Vector2 getNextCell(Direction direction) {
        Vector2 currCell = getCurrentCell();
        Vector2 nextCell = ObjectPool.getInstance().getVector2().set(currCell);
        if(direction != null) {
            float moveX = direction.dx * speed;
            float moveY = direction.dy * speed;
            float nextPosX = currCell.x * tileSize + moveX;
            float nextPosY = currCell.y * tileSize + moveY;
            switch (direction) {
                case LEFT:
                    nextPosY += getHeight() / 2;
                    break;
                case RIGHT:
                    nextPosX += getWidth();
                    nextPosY += getHeight() / 2;
                    break;
                case UP:
                    nextPosX += getWidth() / 2;
                    nextPosY += getHeight();
                    break;
                case DOWN:
                    nextPosX += getWidth() / 2;
                    break;
            }
            nextCell.set((int) (nextPosX / tileSize), (int) (nextPosY / tileSize));
        }
        ObjectPool.getInstance().free(currCell);
        return nextCell;
    }

    public abstract Animation getAnimation();

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        stateTime += Gdx.graphics.getDeltaTime();

        TextureRegion current = getAnimation().getKeyFrame(stateTime, true);
        batch.draw(current, getX(), getY(), getOriginX(), getOriginY(),
                getWidth(), getHeight(), getScaleX(), getScaleY(), rotation);
    }

    public void drawDebug(ShapeRenderer debug) {
        Vector2 currCell = getCurrentCell();
        Vector2 nextCell = getNextCell(direction);
        //current pos
        debug.setColor(1, 1, 0, 1);
        debug.rect(getX(), getY(), getWidth(), getHeight());
        //current cell
        debug.setColor(0, 1, 0, 1);
        debug.rect(currCell.x * tileSize, currCell.y * tileSize, tileSize, tileSize);
        //next cell
        debug.setColor(0, 1, 0, 1);
        debug.rect(nextCell.x * tileSize, nextCell.y * tileSize, tileSize, tileSize);

        ObjectPool.getInstance().free(currCell);
        ObjectPool.getInstance().free(nextCell);
    }

    @Override
    public void setScale(float scaleXY) {
        super.setScale(scaleXY);
        setSize(getWidth() * scaleXY, getHeight() * scaleXY);
        setOrigin(getWidth() / 2, getHeight() / 2);
        speed *= scaleXY;
    }

    private void move() {
        Vector2 currCell = getCurrentCell();
        Vector2 nextCell = getNextCell(direction);

        if(direction != null) {
            float moveX = direction.dx * speed;
            float moveY = direction.dy * speed;

            TiledMapTileLayer.Cell next = maze.getCell((int) nextCell.x, (int) nextCell.y);
            if (checker.isMovable((int) nextCell.x, (int) nextCell.y, next)) {
                moveBy(moveX, moveY);

            } else if (!MathUtils.isEqual(getX(), currCell.x * tileSize) || !MathUtils.isEqual(getY(), currCell.y * tileSize)) {
                float posX = getX() + moveX;
                float posY = getY() + moveY;
                moveX = Math.min(Math.abs(moveX), Math.abs(currCell.x * tileSize - posX)) * direction.dx;
                moveY = Math.min(Math.abs(moveY), Math.abs(currCell.y * tileSize - posY)) * direction.dy;

                if (MathUtils.isZero(moveX) && MathUtils.isZero(moveY)) { //adjust to cell center
                    setPosition(currCell.x * tileSize, currCell.y * tileSize);
                } else {
                    moveBy(moveX, moveY);
                }
            }
        } else {
            setPosition(currCell.x * tileSize, currCell.y * tileSize);
        }

        ObjectPool.getInstance().free(currCell);
        ObjectPool.getInstance().free(nextCell);
    }

}
