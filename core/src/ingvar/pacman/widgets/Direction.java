package ingvar.pacman.widgets;

/**
 * Created by Igor Zubenko on 12.07.2014.
 */
public enum Direction {

    UP(1, 0, 1),
    DOWN(2, 0, -1),
    LEFT(4, -1, 0),
    RIGHT(8, 1, 0);

    public final int value;
    public final int dx;
    public final int dy;

    Direction(int v, int dx, int dy) {
        this.value = v;
        this.dx = dx;
        this.dy = dy;
    }

}
