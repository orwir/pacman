package ingvar.pacman.widgets;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;

/**
 * Created by Igor Zubenko on 12.07.2014.
 */
public class MenuPromo extends WidgetGroup {

    private static final float VELOCITY = 2f;

    private Ghost[] ghosts;
    private Pacman pacman;
    private float moveX;

    public MenuPromo() {
        ghosts = new Ghost[] {new HunterGhost(), new DemonGhost(), new GoblinGhost()};
        pacman = new Pacman();
        float posX = 0;
        for(Ghost ghost : ghosts) {
            ghost.setPosition(posX, 0);
            addActor(ghost);
            posX += ghost.getWidth() + pacman.getWidth() / 2;
        }
        pacman.setPosition(posX, 0);
        addActor(pacman);
        setSize(pacman.getX() + pacman.getWidth(), pacman.getHeight());
        moveX = VELOCITY;

        addAction(Actions.delay(2f, Actions.forever(new Action() {
            @Override
            public boolean act(float delta) {
                if(getX() + getWidth() + pacman.getWidth() < 0) {
                    moveX = VELOCITY;
                    invert(Direction.RIGHT);
                }
                else if(getX() - (getWidth() + pacman.getWidth()) > getStage().getWidth()) {
                    moveX = -VELOCITY;
                    invert(Direction.LEFT);
                }
                moveBy(moveX, 0);
                return true;
            }
        })));
    }

    private void invert(Direction direction) {
        pacman.setDirection(direction);
        for(Ghost ghost : ghosts) {
            ghost.setDirection(direction);
            ghost.setMortal(Direction.LEFT.equals(direction));
        }
    }

}
