package ingvar.pacman.widgets;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import ingvar.pacman.utils.ObjectPool;
import ingvar.pacman.utils.PacmanInteraction;
import ingvar.pacman.utils.Textures;

public class Pacman extends Creature {

    public static final float SPEED = 2;

    private Animation animation;
    private PacmanInteraction interaction;
    private Ghost[] ghosts;

    public Pacman() {
        TextureRegion[] regions = Textures.getInstance().getPacmanFrames();
        animation = new Animation(0.13f, regions);
        speed = SPEED;
        setSize(regions[0].getRegionWidth(), regions[0].getRegionHeight());
        setOrigin(getWidth() / 2, getHeight() / 2);
    }

    @Override
    public boolean setDirection(Direction d) {
        boolean turned = super.setDirection(d);
        if(turned && d != null) {
            switch (getDirection()) {
                case LEFT:
                    setRotation(180);
                    break;
                case RIGHT:
                    setRotation(0);
                    break;
                case UP:
                    setRotation(90);
                    break;
                case DOWN:
                    setRotation(-90);
                    break;
            }
        } else if(d == null) {
            setRotation(0);
        }
        return turned;
    }

    @Override
    public Animation getAnimation() {
        return animation;
    }

    public void initEating(PacmanInteraction handler, Ghost[] enemies) {
        if(maze == null || tiles == null) {
            throw new IllegalStateException("Pacman.initEating must be called after Creature.initMovement.");
        }

        interaction = handler;
        ghosts = enemies;
        addAction(Actions.forever(new Action() {
            @Override
            public boolean act(float delta) {
                Vector2 currCell = getCurrentCell();
                TiledMapTileLayer.Cell curr = maze.getCell((int) currCell.x, (int) currCell.y);
                if (TileType.DOT_SMALL.value == curr.getTile().getId()) {
                    curr.setTile(tiles.getTile(TileType.EMPTY.value));
                    interaction.onEatDot(TileType.DOT_SMALL);
                }
                if(TileType.DOT_BIG.value == curr.getTile().getId()) {
                    curr.setTile(tiles.getTile(TileType.EMPTY.value));
                    interaction.onEatDot(TileType.DOT_BIG);
                }
                Ghost ghost = checkGhost();
                if(ghost != null) {
                    interaction.onEatGhost(ghost);
                }

                ObjectPool.getInstance().free(currCell);
                return true;
            }
        }));
    }

    private Ghost checkGhost() {
        Ghost ghost = null;
        Vector2 pos = getCurrentCell();
        for(Ghost g : ghosts) {
            if(g.isAlive() && g.isMortal()) {
                Vector2 gpos = g.getCurrentCell();
                if (pos.equals(gpos)) {
                    ghost = g;
                    ObjectPool.getInstance().free(gpos);
                    break;
                }
                ObjectPool.getInstance().free(gpos);
            }
        }
        ObjectPool.getInstance().free(pos);
        return ghost;
    }

}
