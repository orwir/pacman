package ingvar.pacman.widgets;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import ingvar.pacman.utils.Dimens;
import ingvar.pacman.utils.Textures;

public class Lives extends Table {

    private TextureRegion pacman;
    private Array<Image> lives;
    private int initialLives;
    private int currLives;

    public Lives(int count) {
        this.pacman = Textures.getInstance().getPacman();
        this.lives = new Array<Image>(initialLives);
        this.initialLives = count;
        this.currLives = count;
        left();

        for(int i = 0; i < initialLives; i++) {
            Image img = new Image(pacman);
            lives.add(img);
            add(img).padLeft(i > 0 ? Dimens.PADDING : 0);
        }
    }

    public void minus() {
        lives.get(--currLives).setVisible(false);
    }

    public int getLives() {
        return currLives;
    }

    public void reset() {
        for(Image image : lives) {
            image.setVisible(true);
        }
        currLives = initialLives;
    }

}
