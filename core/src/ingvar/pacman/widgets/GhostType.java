package ingvar.pacman.widgets;

/**
 * Created by Igor_Zubenko on 14/07/2014.
 */
public enum GhostType {

    HUNTER("hunter"),
    DEMON("demon"),
    GOBLIN("goblin");

    public final String key;

    GhostType(String key) {
        this.key = "ghost-" + key;
    }

}
