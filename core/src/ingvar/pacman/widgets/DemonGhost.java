package ingvar.pacman.widgets;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import ingvar.pacman.ai.CellChecker;
import ingvar.pacman.ai.PathFindingFactory;
import ingvar.pacman.ai.PathNode;
import ingvar.pacman.utils.ObjectPool;

/**
 * Created by Igor Zubenko on 16.07.2014.
 */
public class DemonGhost extends Ghost {

    private Ghost partner;

    public DemonGhost() {
        super(GhostType.DEMON);
        speed = Pacman.SPEED * .80f;
        checker = new CellChecker() {
            @Override
            public boolean isMovable(int x, int y, TiledMapTileLayer.Cell cell) {
                boolean isMovable = cell.getTile().getId() <= TileType.DOOR.value;
                if(isMovable && partner != null && partner.path != null) { //check partner moves
                    PathNode node = partner.path;
                    while(node != null) {
                        Vector2 pacPos = pacman.getCurrentCell();
                        if(node.x != pacPos.x && node.y != pacPos.y && x == node.x && y == node.y) {
                            isMovable = false;
                            break;
                        }
                        node = node.next;
                    }
                }
                return isMovable;
            }
        };
    }

    public void setPartner(Ghost partner) {
        this.partner = partner;
    }

    @Override
    protected void setTrack() {
        Vector2 pacmanPos = pacman.getCurrentCell();
        if(target == null || !target.isSame(pacmanPos)) {
            target = ObjectPool.getInstance().getPathNode().set(pacmanPos.x, pacmanPos.y);
            ObjectPool.getInstance().free(pacmanPos);
            if(path != null) {
                ObjectPool.getInstance().free(path, true);
            }
            path = null;
        }
        if(path == null) {
            Vector2 ghostPos = getCurrentCell();
            PathNode origin = ObjectPool.getInstance().getPathNode().set(ghostPos.x, ghostPos.y);
            path = PathFindingFactory.getInstance().findByAStar(maze, origin, target, checker);
            ObjectPool.getInstance().free(ghostPos);
        }
    }

}
