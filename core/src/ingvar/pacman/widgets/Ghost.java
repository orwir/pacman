package ingvar.pacman.widgets;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Timer;
import ingvar.pacman.ai.CellChecker;
import ingvar.pacman.ai.PathNode;
import ingvar.pacman.utils.GhostInteraction;
import ingvar.pacman.utils.ObjectPool;
import ingvar.pacman.utils.Textures;

public abstract class Ghost extends Creature {

    protected Pacman pacman;
    protected GhostInteraction interaction;
    protected PathNode target;
    protected PathNode path;

    private Animation aniCasual;
    private Animation aniMortal;
    private GhostType type;
    private boolean mortal;
    private boolean alive;

    public Ghost(GhostType type) {
        this.type = type;
        TextureRegion[] regions = Textures.getInstance().getGhost(type);
        aniCasual = new Animation(.13f, regions);
        aniMortal = new Animation(.13f, new TextureRegion[] {regions[0], Textures.getInstance().getMortalGhost()});
        speed = Pacman.SPEED * .75f;
        setSize(regions[0].getRegionWidth(), regions[0].getRegionHeight());
        setOrigin(getWidth() / 2, getHeight() / 2);
        setDirection(null);
        checker = new CellChecker() {
            @Override
            public boolean isMovable(int x, int y, TiledMapTileLayer.Cell cell) {
                return cell.getTile().getId() <= TileType.DOOR.value;
            }
        };
        mortal = false;
        alive  = true;
    }

    public void initHunting(Pacman p, GhostInteraction handler) {
        if(maze == null || tiles == null) {
            throw new IllegalStateException("Ghost.initHunting must be called after Creature.initMovement.");
        }
        this.pacman = p;
        this.interaction = handler;

        addAction(Actions.forever(new Action() {
            @Override
            public boolean act(float delta) {
                if(isAlive()) {
                    chase();
                }
                return true;
            }
        }));
    }

    @Override
    public Animation getAnimation() {
        return mortal ? aniMortal : aniCasual;
    }

    public void setMortal(boolean isMortal) {
        this.mortal = isMortal;
        if(isMortal) {
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    setMortal(false);
                }
            }, 7);
        }
    }

    public boolean isMortal() {
        return mortal;
    }

    public boolean isAlive() {
        return alive;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
        if(!alive) {
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    setAlive(true);
                }
            }, 10);
        }
    }

    public void resetChasing() {
        setDirection(null);
        path = null;
        target = null;
    }

    @Override
    public void drawDebug(ShapeRenderer debug) {
        super.drawDebug(debug);
        if(path != null) {
            debug.setColor(1, 0, 0, 1);
            PathNode node = path;
            while(node != null) {
                debug.rect(node.x * tileSize, node.y * tileSize, tileSize, tileSize);
                node = node.next;
            }
            if(path.d != null) {
                debug.setColor(1, 1, 1, 1);
                debug.rect(path.x * tileSize, path.y * tileSize, tileSize, tileSize);
            }
        }
    }

    /**
     * find target and set path
     */
    protected abstract void setTrack();

    protected void chase() {
        if(isMortal()) {
            setTrack(); //TODO: escape
        } else {
            setTrack();
        }

        Vector2 pos = getCurrentCell();
        if(path != null) {
            if(getDirection() == null || !getDirection().equals(path.d)) {
                setDirection(path.d);
            }
            if(path.isSame(pos)) {
                path = path.next;
            }
        } else {
            setPosition(pos.x * tileSize, pos.y * tileSize);
        }
        Vector2 ppos = pacman.getCurrentCell();
        if(!isMortal() && isAlive() && pos.equals(ppos)) {
            interaction.onEatPacman();
        }
        ObjectPool.getInstance().free(ppos);
        ObjectPool.getInstance().free(pos);
    }

}
