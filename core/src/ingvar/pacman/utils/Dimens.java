package ingvar.pacman.utils;

public class Dimens {

    public static final float BUTTON_WIDTH = 110;
    public static final float PADDING = 16;

    private Dimens() {}

}
