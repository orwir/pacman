package ingvar.pacman.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import ingvar.pacman.widgets.GhostType;

public class Textures {

    public static Textures getInstance() {
        return Holder.HOLDER_INSTANCE;
    }

    private static class Holder {
        public static final Textures HOLDER_INSTANCE = new Textures();
    }

    private Skin skin;
    private TextureAtlas atlas;
    private TiledMap maze;

    private Textures() {
        skin  = new Skin(Gdx.files.internal("skin-default/uiskin.json"), new TextureAtlas("skin-default/uiskin.atlas"));
        atlas = new TextureAtlas("textures.atlas");
        maze  = new TmxMapLoader().load("maze.tmx");
    }

    public Skin getSkin() {
        return skin;
    }

    public TextureRegion getLogo() {
        return atlas.findRegion("logo");
    }

    public TextureRegion getPacman() {
        return atlas.findRegion("pacman");
    }

    public TextureRegion[] getPacmanFrames() {
        return new TextureRegion[] {atlas.findRegion("pacman"), atlas.findRegion("pacman-close")};
    }

    public TextureRegion[] getGhost(GhostType type) {
        return new TextureRegion[] {atlas.findRegion(type.key + "-0"), atlas.findRegion(type.key + "-1")};
    }

    public TextureRegion getMortalGhost() {
        return atlas.findRegion("ghost-mortal-1");
    }

    public TiledMap getMaze() {
        return maze;
    }

    public void dispose() {
        skin.dispose();
    }

}
