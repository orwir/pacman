package ingvar.pacman.utils;

/**
 * Created by Igor_Zubenko on 21/07/2014.
 */
public interface GhostInteraction {

    void onEatPacman();

}
