package ingvar.pacman.utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import ingvar.pacman.ai.PathNode;

/**
 * Created by Igor_Zubenko on 16/07/2014.
 */
public class ObjectPool {

    public static ObjectPool getInstance() {
        return Holder.HOLDER_INSTANCE;
    }

    private static class Holder {
        public static final ObjectPool HOLDER_INSTANCE = new ObjectPool();
    }

    private Pool<Vector2> poolVectors2;
    private Pool<PathNode> poolPathNodes;

    private ObjectPool() {
        poolVectors2 = new Pool<Vector2>() {
            @Override
            protected Vector2 newObject() {
                return new Vector2();
            }
        };
        poolPathNodes = new Pool<PathNode>() {
            @Override
            protected PathNode newObject() {
                return new PathNode();
            }
        };
    }

    public void free(Vector2 vector2) {
        poolVectors2.free(vector2);
    }

    public void free(PathNode pathNode) {
        free(pathNode, false);
    }

    public void free(PathNode pathNode, boolean withHierarchy) {
        poolPathNodes.free(pathNode);
        if(withHierarchy) {
            if (pathNode.prev != null) {
                free(pathNode.prev);
            }
            if (pathNode.next != null) {
                free(pathNode.next);
            }
        }
    }

    public Vector2 getVector2() {
        return poolVectors2.obtain();
    }

    public PathNode getPathNode() {
        return poolPathNodes.obtain();
    }

}
