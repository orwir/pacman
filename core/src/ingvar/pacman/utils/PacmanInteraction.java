package ingvar.pacman.utils;

import ingvar.pacman.widgets.Ghost;
import ingvar.pacman.widgets.TileType;

/**
 * Created by Igor_Zubenko on 21/07/2014.
 */
public interface PacmanInteraction {

    void onEatDot(TileType type);

    void onEatGhost(Ghost ghost);

}
