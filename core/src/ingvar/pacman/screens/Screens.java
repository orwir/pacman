package ingvar.pacman.screens;

import ingvar.pacman.Pacman;

import java.util.HashMap;
import java.util.Map;

public class Screens {

    public static enum Type {
        MENU,
        HELP,
        LADDER,
        GAME;
    }

    public static Screens getInstance() {
        return Holder.HOLDER_INSTANCE;
    }

    private static class Holder {
        public static final Screens HOLDER_INSTANCE = new Screens();
    }

    private Map<Type, AbstractScreen> screens;

    private Screens() {
        screens = new HashMap<Type, AbstractScreen>();
    }

    public void init(Pacman game) {
        screens.put(Type.MENU, new MenuScreen().preInit(game));
        screens.put(Type.HELP, new HelpScreen().preInit(game));
        screens.put(Type.LADDER, new LadderScreen().preInit(game));
        screens.put(Type.GAME, new GameScreen().preInit(game));
    }

    public void dispose() {
        for(Type type : Type.values()) {
            screens.get(type).dispose();
        }
    }

    public AbstractScreen getScreen(Type type) {
        return screens.get(type);
    }

}
