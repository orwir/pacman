package ingvar.pacman.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import ingvar.pacman.utils.Dimens;

public class HelpScreen extends AbstractScreen {

    private Stage stage;

    @Override
    public void render(float delta) {
        super.render(delta);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    protected void init() {
        stage = new Stage(new ScreenViewport());

        Table hud = new Table(skin);
        stage.addActor(hud);
        hud.setFillParent(true);

        List<String> lstControls = new List<String>(skin);
        lstControls.getItems().add("arrows - move");
        lstControls.getItems().add("P - pause game");
        lstControls.getItems().add("R - restart game");
        lstControls.setHeight(3 * skin.get(List.ListStyle.class).font.getLineHeight());
        Button btnBack = new TextButton("Back", skin);

        hud.add(lstControls).padBottom(Dimens.PADDING).row();
        hud.add(btnBack);

        btnBack.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                changeScreen(Screens.Type.MENU);
            }
        });
    }

}
