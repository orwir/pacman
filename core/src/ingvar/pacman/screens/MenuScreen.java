package ingvar.pacman.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import ingvar.pacman.utils.Dimens;
import ingvar.pacman.utils.Textures;
import ingvar.pacman.widgets.MenuPromo;

public class MenuScreen extends AbstractScreen {

    private Stage stage;

    @Override
    public void render(float delta) {
        super.render(delta);
        stage.act(delta);
        stage.draw();
        //Table.drawDebug(stage);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    protected void init() {
        stage = new Stage(new ScreenViewport());

        Image imgLogo = new Image(Textures.getInstance().getLogo());
        Button btnPlay = new TextButton("Play game", skin);
        Button btnHelp = new TextButton("Help", skin);
        Button btnLadder = new TextButton("Hall of Fame", skin);
        WidgetGroup wdgMenuPromo = new MenuPromo();

        Table hud = new Table(skin);
        stage.addActor(hud);
        hud.setFillParent(true);
        //hud.debug();

        hud.add(imgLogo).padBottom(Dimens.PADDING).row();
        hud.add(btnPlay).width(Dimens.BUTTON_WIDTH).padBottom(Dimens.PADDING).row();
        hud.add(btnHelp).width(Dimens.BUTTON_WIDTH).padBottom(Dimens.PADDING).row();
        //hud.add(btnLadder).width(Dimens.BUTTON_WIDTH);

        stage.addActor(wdgMenuPromo);
        wdgMenuPromo.setPosition(-wdgMenuPromo.getWidth(), Dimens.PADDING);

        btnPlay.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                changeScreen(Screens.Type.GAME);
            }
        });
        btnHelp.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                changeScreen(Screens.Type.HELP);
            }
        });
        btnLadder.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                changeScreen(Screens.Type.LADDER);
            }
        });
    }

}
