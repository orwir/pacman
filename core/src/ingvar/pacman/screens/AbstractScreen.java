package ingvar.pacman.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import ingvar.pacman.Pacman;
import ingvar.pacman.utils.Textures;

public abstract class AbstractScreen implements Screen {

    private Pacman game;
    protected Skin skin;

    public void changeScreen(Screens.Type type) {
        AbstractScreen screen = Screens.getInstance().getScreen(type);
        game.setScreen(screen);
    }

    @Override
    public void render(float delta) {
        GL20 gl = Gdx.gl;
        gl.glClearColor(0, 0, 0, 1);
        gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    protected AbstractScreen preInit(Pacman game) {
        this.game = game;
        this.skin = Textures.getInstance().getSkin();
        init();
        return this;
    }

    protected abstract void init();

}
