package ingvar.pacman.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileSet;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import ingvar.pacman.utils.*;
import ingvar.pacman.widgets.*;

public class GameScreen extends AbstractScreen {

    private static final int INIT_LIVES = 3;
    private static final int SCORE_DOT_SMALL = 10;
    private static final int SCORE_DOT_BIG = 50;
    private static final int SCORE_GHOST = 200;

    private static final float MAP_SCALE = .81f;

    private Stage stage;
    private Lives lives;
    private Label lblScore;
    private OrthogonalTiledMapRenderer mazeRenderer;
    private TiledMap mazeMap;
    private Pacman pacman;
    private int score;
    private Ghost[] ghosts;
    private boolean isRunning;
    private boolean isPaused;

    private Array<Vector2> smallDots;
    private Array<Vector2> bigDots;
    private int eated;
    private int dotsCount;

    private ShapeRenderer debug;

    public void restart() {
        isRunning = false;
        isPaused  = false;

        final float tileSize = 32f * MAP_SCALE;
        pacman.setPosition(13 * tileSize, 5 * tileSize);
        score = 0;
        eated = 0;
        pacman.setDirection(null);
        int gpos = 14;
        for(Ghost ghost : ghosts) {
            ghost.setPosition(gpos++ * (32 * MAP_SCALE), 15 * (32 * MAP_SCALE));
            ghost.resetChasing();
        }

        TiledMapTileLayer layer = (TiledMapTileLayer) mazeMap.getLayers().get(0);
        TiledMapTileSet tiles = mazeMap.getTileSets().getTileSet(0);
        for(Vector2 pos : smallDots) {
            layer.getCell((int)pos.x, (int)pos.y).setTile(tiles.getTile(TileType.DOT_SMALL.value));
        }
        for(Vector2 pos : bigDots) {
            layer.getCell((int)pos.x, (int)pos.y).setTile(tiles.getTile(TileType.DOT_BIG.value));
        }

        lives.reset();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        mazeRenderer.render();
        if(isRunning && !isPaused) {
            stage.act(delta);
        }
        stage.draw();

        /*debug.begin(ShapeRenderer.ShapeType.Line);
        pacman.drawDebug(debug);
        for(Ghost ghost : ghosts) {
            ghost.drawDebug(debug);
        }
        debug.end();*/
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(stage);
        restart();
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    protected void init() {
        isRunning = false;
        isPaused  = false;
        debug = new ShapeRenderer();

        stage = new Stage(new ScreenViewport());
        lives = new Lives(INIT_LIVES);
        Label lblScoreText = new Label("SCORE", skin);
        lblScore = new Label("0", skin);

        Table hud = new Table(skin);
        stage.addActor(hud);
        hud.setFillParent(true);
        hud.top().left();
        hud.padLeft(Dimens.PADDING).padTop(Dimens.PADDING).padBottom(Dimens.PADDING);

        hud.add(lblScoreText).padRight(Dimens.PADDING);
        hud.add(lblScore);
        hud.add(new Label("", skin)).expandX();
        hud.add(lives).right().padRight(Dimens.PADDING);

        mazeMap = Textures.getInstance().getMaze();
        mazeRenderer = new OrthogonalTiledMapRenderer(mazeMap, MAP_SCALE);
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        mazeRenderer.setView(camera);

        final float tileSize = 32f * MAP_SCALE;
        pacman = new Pacman();
        pacman.setScale(MAP_SCALE);
        pacman.setPosition(13 * tileSize, 5 * tileSize);
        pacman.initMovement((TiledMapTileLayer) mazeMap.getLayers().get(0), mazeMap.getTileSets().getTileSet(0), tileSize);

        ghosts = new Ghost[] {new HunterGhost(), new DemonGhost(), new GoblinGhost()};
        int pos = 14;
        for(Ghost ghost : ghosts) {
            stage.addActor(ghost);
            ghost.setScale(MAP_SCALE);
            ghost.setPosition(pos++ * (32 * MAP_SCALE), 15 * (32 * MAP_SCALE));
            ghost.initMovement((TiledMapTileLayer) mazeMap.getLayers().get(0), mazeMap.getTileSets().getTileSet(0), tileSize);
            ghost.initHunting(pacman, new GhostInteraction() {
                @Override
                public void onEatPacman() {
                    lives.minus();
                    if(lives.getLives() > 0) {
                        pacman.setPosition(13 * tileSize, 5 * tileSize);
                        int pos = 14;
                        for(Ghost ghost : ghosts) {
                            ghost.setPosition(pos++ * (32 * MAP_SCALE), 15 * (32 * MAP_SCALE));
                            ghost.resetChasing();
                        }
                        pacman.setDirection(null);
                        isRunning = false;
                    } else {
                        //TODO: GAME OVER
                        restart();
                    }
                }
            });
        }
        ((DemonGhost) ghosts[1]).setPartner(ghosts[0]);
        stage.addActor(pacman);

        pacman.initEating(new PacmanInteraction() {
            @Override
            public void onEatDot(TileType type) {
                if(TileType.DOT_SMALL.equals(type)) {
                    score += SCORE_DOT_SMALL;
                    eated++;
                }
                else if(TileType.DOT_BIG.equals(type)) {
                    score += SCORE_DOT_BIG;
                    eated++;

                    for(Ghost g : ghosts) {
                        g.setMortal(true);
                    }
                }
                lblScore.setText(Integer.toString(score));
                if(eated == dotsCount) {
                    //TODO: GAME OVER
                    restart();
                }
            }

            @Override
            public void onEatGhost(Ghost ghost) {
                score += SCORE_GHOST;
                lblScore.setText(Integer.toString(score));

                ghost.setAlive(false);
                ghost.setPosition(14 * (32 * MAP_SCALE), 15 * (32 * MAP_SCALE));
                ghost.resetChasing();
            }
        }, ghosts);

        final Movement movement = new Movement();
        stage.addAction(Actions.forever(movement));

        stage.addListener(new ClickListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if(!isRunning) {
                    isRunning = true;
                }
                switch (keycode) {
                    case Input.Keys.LEFT:
                        movement.direction = Direction.LEFT;
                        break;
                    case Input.Keys.RIGHT:
                        movement.direction = Direction.RIGHT;
                        break;
                    case Input.Keys.UP:
                        movement.direction = Direction.UP;
                        break;
                    case Input.Keys.DOWN:
                        movement.direction = Direction.DOWN;
                        break;
                    case Input.Keys.R:
                        restart();
                        break;
                    case Input.Keys.P:
                        isPaused = !isPaused;
                        break;
                }
                return true;
            }

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                movement.direction = null;
                return true;
            }

        });

        smallDots = new Array<Vector2>();
        bigDots = new Array<Vector2>();
        TiledMapTileLayer layer = (TiledMapTileLayer) mazeMap.getLayers().get(0);
        for(int x = 0; x < layer.getWidth(); x++) {
            for(int y = 0; y < layer.getHeight(); y++) {
                TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                if(cell.getTile().getId() == TileType.DOT_BIG.value) {
                    bigDots.add(ObjectPool.getInstance().getVector2().set(x, y));
                    dotsCount++;
                }
                else if(cell.getTile().getId() == TileType.DOT_SMALL.value) {
                    smallDots.add(ObjectPool.getInstance().getVector2().set(x, y));
                    dotsCount++;
                }
            }
        }
    }

    private class Movement extends Action {

        Direction direction;

        @Override
        public boolean act(float delta) {
            if(direction != null) {
                pacman.setDirection(direction);
            }
            return true;
        }
    }

}
