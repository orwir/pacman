package ingvar.pacman;

import com.badlogic.gdx.Game;
import ingvar.pacman.screens.Screens;
import ingvar.pacman.utils.Textures;

public class Pacman extends Game {
	
	@Override
	public void create () {
        Screens.getInstance().init(this);

        setScreen(Screens.getInstance().getScreen(Screens.Type.MENU));
	}

    @Override
    public void dispose() {
        super.dispose();
        Textures.getInstance().dispose();
        Screens.getInstance().dispose();
    }
}
