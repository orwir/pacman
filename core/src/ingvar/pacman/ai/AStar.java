package ingvar.pacman.ai;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.utils.Array;
import ingvar.pacman.utils.ObjectPool;
import ingvar.pacman.widgets.Direction;

/**
 * Created by Igor_Zubenko on 16/07/2014.
 */
class AStar {

    private Array<PathNode> explored;
    private Array<PathNode> frontier;

    private TiledMapTileLayer maze;
    private PathNode origin;
    private PathNode target;
    private CellChecker checker;

    public AStar() {
        explored = new Array<PathNode>(false, 1000);
        frontier = new Array<PathNode>(false, 1000);
    }

    public AStar init(TiledMapTileLayer maze, PathNode origin, PathNode target, CellChecker checker) {
        //clear old
        explored.clear();
        frontier.clear();
        //init new
        this.maze = maze;
        this.origin = origin;
        this.target = target;
        this.checker = checker;
        return this;
    }

    /**
     * return to target
     * First element is a target and prev elements is path to origin
     * @return target PathNode
     */
    public PathNode find() {
        rate(origin);
        frontier.add(origin);

        PathNode next = null;
        while (frontier.size > 0) {
            next = popMin();
            if(next.x == target.x && next.y == target.y) {
                break;
            }
            explore(next);
        }

        return next;
    }

    /**
     * Explore nodes around current node
     * @param curr current node
     */
    private void explore(PathNode curr) {
        explored.add(curr);

        for(Direction d : Direction.values()) {
            PathNode node = initNode(curr, d);
            if(node.x < 0 || node.x >= maze.getWidth() || node.y < 0 || node.y >= maze.getHeight()) {
                ObjectPool.getInstance().free(node);
                continue;
            }
            TiledMapTileLayer.Cell cell = maze.getCell(node.x, node.y);
            if(!checker.isMovable(node.x, node.y, cell)) {
                ObjectPool.getInstance().free(node);
                continue;
            }
            if(explored.contains(node, false)) {
                ObjectPool.getInstance().free(node);
                continue;
            }
            rate(node);
            frontier.add(node);
        }
    }

    /**
     * find and remove node with best rate (lower)
     * @return node with min rate
     */
    private PathNode popMin() {
        PathNode min = null;
        for(PathNode cmp : frontier) {
            if(min == null || cmp.rate < min.rate) {
                min = cmp;
            }
        }
        frontier.removeValue(min, true);
        return min;
    }

    /**
     * rate node for summary of params:
     * 1. how closer to target
     * 2. how many steps you gone from origin
     * @param node node for rate
     */
    private void rate(PathNode node) {
        int ct = Math.abs(target.x - node.x) + Math.abs(target.y - node.y);
        node.rate = ct + node.count;
    }

    /**
     * create neighbour node for source
     * @param source origin target
     * @param side side of origin
     */
    private PathNode initNode(PathNode source, Direction side) {
        PathNode node = ObjectPool.getInstance().getPathNode();
        node.x = source.x;
        node.y = source.y;
        switch (side) {
            case LEFT:
                node.x--;
                break;
            case RIGHT:
                node.x++;
                break;
            case UP:
                node.y++;
                break;
            case DOWN:
                node.y--;
                break;
        }
        node.prev = source;
        node.count = source.count + 1;

        return node;
    }

}
