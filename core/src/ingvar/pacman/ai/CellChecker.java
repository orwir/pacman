package ingvar.pacman.ai;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;

/**
 * Created by Igor_Zubenko on 18/07/2014.
 */
public interface CellChecker {

    boolean isMovable(int x, int y, TiledMapTileLayer.Cell cell);

}
