package ingvar.pacman.ai;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;
import ingvar.pacman.widgets.Direction;

/**
 * Created by Igor_Zubenko on 15/07/2014.
 */
public class PathNode implements Pool.Poolable {

    public int x;
    public int y;
    public int rate; //less is better
    public int count; //previous steps
    public PathNode prev; //previous node on the path

    public PathNode next; //next node on the path
    public Direction d; //direction to this node

    public boolean isSame(Vector2 pos) {
        return x == (int) pos.x && y == (int) pos.y;
    }

    public PathNode set(int x, int y) {
        this.x = x;
        this.y = y;

        return this;
    }

    public PathNode set(float x, float y) {
        return set((int) x, (int) y);
    }

    @Override
    public void reset() {
        x = -1;
        y = -1;
        rate  = 0;
        count = 0;
        prev  = null;
        next  = null;
        d = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        //if (o == null || getClass() != o.getClass()) return false;

        PathNode pathNode = (PathNode) o;

        if (x != pathNode.x) return false;
        if (y != pathNode.y) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}
