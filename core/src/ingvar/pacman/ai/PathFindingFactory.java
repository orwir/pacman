package ingvar.pacman.ai;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.utils.Pool;
import ingvar.pacman.widgets.Direction;

/**
 * Created by Igor_Zubenko on 16/07/2014.
 */
public class PathFindingFactory {

    public static PathFindingFactory getInstance() {
        return Holder.HOLDER_INSTANCE;
    }

    private static class Holder {
        public static final PathFindingFactory HOLDER_INSTANCE = new PathFindingFactory();
    }

    private Pool<AStar> poolA;

    private PathFindingFactory() {
        poolA = new Pool<AStar>() {
            @Override
            protected AStar newObject() {
                return new AStar();
            }
        };
    }

    /**
     * Find path and return PathNode from origin to target
     *
     * @param maze where we must find the way
     * @param origin start node
     * @param target end node
     * @param checker cell checker
     * @return first step of path
     */
    public PathNode findByAStar(TiledMapTileLayer maze, PathNode origin, PathNode target, CellChecker checker) {
        PathNode result = origin;
        if(!origin.equals(target)) {
            AStar algo = poolA.obtain();
            PathNode next = algo.init(maze, origin, target, checker).find();
            if(next != null) {
                poolA.free(algo);
                if(next.prev != null) {
                    if (next.x < next.prev.x) {
                        next.d = Direction.LEFT;
                    } else if (next.x > next.prev.x) {
                        next.d = Direction.RIGHT;
                    } else if (next.y < next.prev.y) {
                        next.d = Direction.DOWN;
                    } else if (next.y > next.prev.y) {
                        next.d = Direction.UP;
                    }
                }
                result = prepareSolve(next);
            }
        }

        return result;
    }

    /**
     * move to first node and fill next node and direction
     * @param node
     * @return
     */
    private PathNode prepareSolve(PathNode node) {
        PathNode prev = node.prev;
        if(prev != null) {
            prev.next = node;
            if(prev.x < node.x) {
                prev.d = Direction.RIGHT;
            }
            else if(prev.x > node.x) {
                prev.d = Direction.LEFT;
            }
            else if(prev.y < node.y) {
                prev.d = Direction.UP;
            }
            else if(prev.y > node.y) {
                prev.d = Direction.DOWN;
            }
            return prepareSolve(prev);
        } else {
            return node;
        }
    }

}
