package ingvar.pacman.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import ingvar.pacman.Pacman;

public class DesktopLauncher {
	public static void main (String[] arg) {
        //createTextures();

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width  = 800;
        config.height = 600;
		new LwjglApplication(new Pacman(), config);
	}

    private static void createTextures() {
        TexturePacker.Settings settings = new TexturePacker.Settings();
        TexturePacker.process(settings, "../../images/textures", ".", "textures");
        settings.pot = false;
        settings.paddingX = 0;
        settings.paddingY = 0;
        TexturePacker.process(settings, "../../images/maze", ".", "maze");
    }

}
